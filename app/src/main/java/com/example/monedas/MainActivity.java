package com.example.monedas;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Spinner spinner;
    private EditText txtMXN;
    private TextView lblTotal;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnExit;
    double total = 0;
    int pos = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtMXN = (EditText) findViewById(R.id.txtMoneda);
        lblTotal = (TextView) findViewById(R.id.lblMoneda);
        btnCalcular = (Button) findViewById(R.id.btnCambio);
        btnLimpiar = (Button) findViewById(R.id.btnBorrar);
        btnExit = (Button) findViewById(R.id.btnSalir);

        spinner = (Spinner) findViewById(R.id.spnMonedas);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MainActivity.this,android.R.layout.simple_expandable_list_item_1,getResources().getStringArray(R.array.monedas));
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(final AdapterView<?> parent, View view, int position, long id) {
                String nombre = parent.getItemAtPosition(position).toString();
                long idx = parent.getItemIdAtPosition(position);

                try{
                    if(position == 0){
                        pos = 0;
                    }
                    else{
                        switch (position){
                            case 1:
                                pos = 1;
                                break;
                            case 2:
                                pos = 2;
                                break;
                            case 3:
                                pos = 3;
                                break;
                            case 4:
                                pos = 4;
                                break;
                            default:
                                pos = 0;
                                break;
                        }
                    }
                }catch (NumberFormatException e){
                    lblTotal.setText("  Resultado: error en operacion");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtMXN.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"Favor de ingresar un valor antes de calcular",Toast.LENGTH_LONG).show();
                }else {
                    int MXN = Integer.parseInt(txtMXN.getText().toString());
                    if (pos == 0) {
                        lblTotal.setText("  Debes Seleccionar Una Maneda");
                    } else {
                        switch (pos) {
                            case 1:
                                total = MXN * 0.053570;
                                lblTotal.setText("  TOTAL: " + total + " USD");
                                break;
                            case 2:
                                total = MXN * 0.048290;
                                lblTotal.setText("  TOTAL: " + total + " EUR");
                                break;
                            case 3:
                                total = MXN * 0.069990;
                                lblTotal.setText("  TOTAL: " + total + " CAD");
                                break;
                            case 4:
                                total = MXN * 0.041160;
                                lblTotal.setText("  TOTAL: " + total + " GBP");
                                break;
                        }
                    }
                }
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(txtMXN.getText().toString().matches("")){
                    Toast.makeText(MainActivity.this,"No hay nada que LIMPIAR",Toast.LENGTH_SHORT).show();
                    spinner.setSelection(0);
                }
                else{
                    txtMXN.setText("");
                    lblTotal.setText("");
                    spinner.setSelection(0);
                }
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
    public void proceso(){

    }
}
